organization  := "de.heikoseeberger" 
name          := "scala-train-demo"
version       := "2.0.0"

scalaVersion  := "2.11.8"
scalacOptions  := List(
  "-unchecked",
  "-deprecation",
  "-target:jvm-1.8"
)
libraryDependencies ++= List(
  "org.apache.logging.log4j" %  "log4j-api"  % "2.3",
  "org.scalactic"            %% "scalactic"  % "2.2.5",
  "org.scala-lang"           % "scala-reflect" % "2.11.8",
  "org.scala-lang"           % "scala-library" % "2.11.8",
  "org.scala-lang.modules"   %% "scala-xml" % "1.0.5",
  "org.scalatest"            %% "scalatest" % "2.2.5" % "test",
  "org.scalacheck"           %% "scalacheck" % "1.12.4" % "test"
 )

import scalariform.formatter.preferences._
SbtScalariform.autoImport.preferences := SbtScalariform.autoImport.preferences.value
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(AlignSingleLineCaseStatements.MaxArrowIndent, 100)
  .setPreference(DoubleIndentClassDeclaration, true)

initialCommands := "import de.heikoseeberger.scalatraindemo._"