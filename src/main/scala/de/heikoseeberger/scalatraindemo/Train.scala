package de.heikoseeberger.scalatraindemo

import scala.collection.immutable.Seq

case class Train(kind: String, number: Int, schedule: Seq[Stop]) {
  require(kind.nonEmpty, "kind must not be empty")
  require(schedule.size >= 2, "schedule must have at least two stops")
  require(schedule.distinct == schedule, "schedule must not contain duplicate stations!")
  // TODO schedule mut be increasing in time

  def stations: Seq[Station] = schedule.map(_.station)
  //def stations: Seq[Station] = schedule.map(schedule => schedule.station)
  //def stations: Seq[Station] = schedule.map((schedule: Stop) => schedule.station)
}

final case class Stop(station: Station, arivalTime: Time, departureTime: Time) {
  //require(arivalTime < departureTime, "arrivalTime mut be before departureTime!")
}

final case class Station(name: String) {
  require(name.nonEmpty, "name must not be empty")
}
