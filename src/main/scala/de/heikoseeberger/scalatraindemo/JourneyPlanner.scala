package de.heikoseeberger.scalatraindemo

import scala.collection.immutable.Seq

class JourneyPlanner(trains: Set[Train]) {
  def stations: Set[Station] = trains.flatMap(_.stations)

  def trainsAt(station: Station): Set[Train] =
    //trains.filter(_.stations.contains(station))
    //trains.filter(train => train.stations.contains(station)
    trains.filter((train: Train) => train.stations.contains(station))

  def departures(station: Station): Set[(Time, Train)] =
    trains.flatMap { train =>
      train.schedule
        .filter(stop => stop.station == station)
        .map(stop => (stop.departureTime, train))
    }

  def departuresAt(station: Station): Set[(Time, Train)] = for {
    train <- trains
    stop <- train.schedule if stop.station == station
  } yield (stop.departureTime, train)

}
