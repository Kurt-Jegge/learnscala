package de.heikoseeberger.scalatraindemo

import org.scalatest.{ Matchers, WordSpec }
import scala.collection.immutable.Seq

class TrainSpec extends WordSpec with Matchers {

  "Creating a Train" should {
    "throw an IllegalArgumentException for an empty kind" in {
      an[IllegalArgumentException] should be thrownBy Train("", 888, Seq(Station("Bern")))
    }
  }

  "Creating a Station" should {
    "throw an IllegalArgumentException for an empty name" in {
      an[IllegalArgumentException] should be thrownBy Station("")
    }
  }
}
